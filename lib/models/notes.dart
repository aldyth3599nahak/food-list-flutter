import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';

class Notes {
  int id;
  String name;
  int price;
  Text description;
  String category;

  Notes({this.id, this.name, this.price, this.description, this.category});

  factory Notes.fromJson(Map<String, dynamic> item) {
    return Notes(
        id: item['id'],
        name: item['name'],
        price: item['price'],
        description: item['description'],
        category: item['category']);
  }

  Map<String, dynamic> toJson() {
    return {'category': category, 'name': name, 'description': description, 'price': price};
  }

  @override
  String toString() {
    return 'Notes{id: $id, name: $name, price: $price, description: $description, category: $category}';
  }

  List<Notes> notesFromJson(String jsonData) {
    final data = jsonDecode(jsonData);
    return List<Notes>.from(data.map((item) => Notes.fromJson(item)));
  }

  String notesToJson(Notes data) {
    final jsonData = data.toJson();
    return json.encode(jsonData);
  }
}
